package com.jfinal.weixin.sdk.cache;

public class RedisAccessTokenCache implements IAccessTokenCache {
    //    /**
//     * 微信token缓存
//     *
//     * @author Jason Xie on 2018/2/9.
//     */
//    @Service
//    @Slf4j
//    public class WechatAccessToken extends DefaultCache {
//        // 静态方法为无法注入服务的实体类提供访问 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//        private static WechatAccessToken bean;
//        public static WechatAccessToken instance() {
//            if (Objects.isNull(bean)) bean = InitConfig.Beans.appContext.getAppContext().getBean(WechatAccessToken.class);
//            return bean;
//        }
//        // 静态方法为无法注入服务的实体类提供访问 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//
//        /**
//         * 缓存key前缀
//         */
//        private static final String KEY = "WechatAccessToken:";
//
//        public String get(final String key) {
//            return redisTemplate.opsForValue().get(KEY.concat(key));
//        }
//
//        public void set(final String key, final String jsonValue) {
//            final BoundValueOperations<String, String> token = redisTemplate.boundValueOps(KEY.concat(key));
//            token.set(jsonValue);
//            token.expire(7200 - 60, TimeUnit.SECONDS);
//        }
//        public void remove(final String key) {
//            redisTemplate.delete(KEY.concat(key));
//        }
//    }
//
//    private final WechatAccessToken token;
//
//    public RedisAccessTokenCache() {
//        token = WechatAccessToken.instance();
//    }
//
//    @Override
//    public String get(final String key) {
//        return token.get(key);
//    }
//
//    @Override
//    public void set(final String key, final String jsonValue) {
//        token.set(key, jsonValue);
//    }
//
//    @Override
//    public void remove(final String key) {
//        token.delete(key);
//    }
    public RedisAccessTokenCache() {
    }

    @Override
    public String get(final String key) {
        return null;
    }

    @Override
    public void set(final String key, final String jsonValue) {
    }

    @Override
    public void remove(final String key) {
    }
}
