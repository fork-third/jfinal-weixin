package com.jfinal.weixin.sdk.kit;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机工具，用于生成 nonce_str 等随机字符串
 */
public class RandomKit {

    private static final char[] CHAR_ARRAY = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    /**
     * 生成指定长度的随机字符串
     */
    public static String gen(int len) {
        StringBuilder salt = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            salt.append(CHAR_ARRAY[ThreadLocalRandom.current().nextInt(CHAR_ARRAY.length)]);
        }
        return salt.toString();
    }

    /**
     * 微信支付
     */
    public static String genNonceStr() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}

