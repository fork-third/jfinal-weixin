/**
 * Copyright (c) 2011-2014, L.cm 卢春梦 (qq596392912@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.wxaapp.msg;

import com.alibaba.fastjson.JSON;

/**
 * 消息模型
 *
 * fastjson本身支持首字母大写的转换
 *
 * Jackson采用注解的形式支持
 *
 * @author L.cm
 *
 */
public class MsgModel {
    @XPath("//ToUserName")
    private String toUserName;
    @XPath("//FromUserName")
    private String fromUserName;
    @XPath("//CreateTime")
    private Integer createTime;
    @XPath("//MsgType")
    private String msgType;

    // 文本消息
    @XPath("//Content")
    private String content;
    @XPath("//MsgId")
    private Long msgId;

    // 图片消息
    @XPath("//PicUrl")
    private String picUrl;
    @XPath("//MediaId")
    private String mediaId;

    // 事件消息
    @XPath("//Event")
    private String event;
    @XPath("//SessionFrom")
    private String sessionFrom;

    // 小程序客服卡片消息
    @XPath("//Title")
    private String Title;
    @XPath("//AppId")
    private String AppId;
    @XPath("//PagePath")
    private String PagePath;
    @XPath("//ThumbUrl")
    private String ThumbUrl;
    @XPath("//ThumbMediaId")
    private String ThumbMediaId;

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public Integer getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Integer createTime) {
        this.createTime = createTime;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getSessionFrom() {
        return sessionFrom;
    }

    public void setSessionFrom(String sessionFrom) {
        this.sessionFrom = sessionFrom;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAppId() {
        return AppId;
    }

    public void setAppId(String appId) {
        AppId = appId;
    }

    public String getPagePath() {
        return PagePath;
    }

    public void setPagePath(String pagePath) {
        PagePath = pagePath;
    }

    public String getThumbUrl() {
        return ThumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        ThumbUrl = thumbUrl;
    }

    public String getThumbMediaId() {
        return ThumbMediaId;
    }

    public void setThumbMediaId(String thumbMediaId) {
        ThumbMediaId = thumbMediaId;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
