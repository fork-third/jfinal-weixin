package com.jfinal.wxaapp.api;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.utils.HttpUtils;

import java.util.HashMap;

/**
 * @author Javen
 * SOTER 生物认证秘钥签名验证
 */
public class WxaVerifySignatureApi {
    private static String verifySignatureUrl = "https://api.weixin.qq.com/cgi-bin/soter/verify_signature?access_token=";

    /**
     * 生物认证秘钥签名验证
     *
     * @param openId        用户 openid
     * @param jsonString    通过 wx.startSoterAuthentication 成功回调获得的 resultJSON 字段
     * @param jsonSignature 通过 wx.startSoterAuthentication 成功回调获得的 resultJSONSignature 字段
     * @return {ApiResult}
     */
    public static ApiResult verifySignature(String openId, String jsonString, String jsonSignature) {
        String accessToken = WxaAccessTokenApi.getAccessTokenStr();
        HashMap<Object, Object> map = Maps.newHashMap();
        map.put("openid", openId);
        map.put("json_string", jsonString);
        map.put("json_signature", jsonSignature);
        String jsonResult = HttpUtils.post(verifySignatureUrl + accessToken, JSON.toJSONString(map));
        return new ApiResult(jsonResult);
    }
}
