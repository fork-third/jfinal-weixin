package com.jfinal.wxaapp.api;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.utils.HttpUtils;

import java.util.HashMap;

/**
 * 微信小程序客服消息界面向客户端发送输入状态
 *
 * @author: 山东小木
 */
public class WxaTypingApi {
    private static String setTypingUrl = "https://api.weixin.qq.com/cgi-bin/message/custom/typing?access_token=";

    /**
     * 发送commond
     *
     * @param openId
     * @param commond
     * @return
     */
    private static ApiResult sendCommond(String openId, String commond) {
        String accessToken = WxaAccessTokenApi.getAccessTokenStr();
        HashMap<Object, Object> map = Maps.newHashMap();
        map.put("touser", openId);
        map.put("command", commond);
        String jsonResult = HttpUtils.post(setTypingUrl + accessToken, JSON.toJSONString(map));
        return new ApiResult(jsonResult);
    }

    /**
     * 下发输入状态
     *
     * @param openId
     * @return
     */
    public static ApiResult setTyping(String openId) {
        return sendCommond(openId, "Typing");
    }

    /**
     * 取消输入状态
     *
     * @param openId
     * @return
     */
    public static ApiResult cancelTyping(String openId) {
        return sendCommond(openId, "CancelTyping");
    }
}
