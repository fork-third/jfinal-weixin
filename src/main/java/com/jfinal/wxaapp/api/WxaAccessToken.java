/**
 * Copyright (c) 2011-2014, L.cm 卢春梦 (qq596392912@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.wxaapp.api;

import com.alibaba.fastjson.JSON;
import com.jfinal.weixin.sdk.api.ReturnCode;
import com.jfinal.weixin.sdk.utils.RetryUtils.ResultCheck;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

@SuppressWarnings("unchecked")
public class WxaAccessToken implements ResultCheck, Serializable {
    private static final long serialVersionUID = 4628857059125205404L;

    private String access_token;    // 正确获取到 access_token 时有值
    private Integer expires_in;     // 正确获取到 access_token 时有值
    private Integer errcode;        // 出错时有值
    private String errmsg;          // 出错时有值
    private Long expiredTime;       // 正确获取到 access_token 时有值，存放过期时间
    private String json;

    public WxaAccessToken(String jsonStr) {
        this.json = jsonStr;

        try {
            Map<String, Object> temp = JSON.parseObject(jsonStr, Map.class);
            access_token = (String) temp.get("access_token");
            expires_in = (Integer) temp.get("expires_in");
            errcode = (Integer) temp.get("errcode");
            errmsg = (String) temp.get("errmsg");

            if (Objects.nonNull(expires_in))
                expiredTime = System.currentTimeMillis() + ((expires_in - 5) * 1000);
            // 用户缓存时还原
            if (temp.containsKey("expiredTime")) {
                expiredTime = (Long) temp.get("expiredTime");
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getJson() {
        return json;
    }

    public String getCacheJson() {
        Map<String, Object> temp = JSON.parseObject(json, Map.class);
        temp.put("expiredTime", expiredTime);
        temp.remove("expires_in");
        return JSON.toJSONString(temp);
    }

    public boolean isAvailable() {
        if (Objects.isNull(expiredTime))
            return false;
        if (Objects.nonNull(errcode))
            return false;
        if (expiredTime < System.currentTimeMillis())
            return false;
        return Objects.nonNull(access_token);
    }

    public String getAccessToken() {
        return access_token;
    }

    public Integer getExpiresIn() {
        return expires_in;
    }

    public Long getExpiredTime() {
        return expiredTime;
    }

    public Integer getErrorCode() {
        return errcode;
    }

    public String getErrorMsg() {
        if (Objects.nonNull(errcode)) {
            String result = ReturnCode.get(errcode);
            if (Objects.nonNull(result))
                return result;
        }
        return errmsg;
    }

    @Override
    public boolean matching() {
        return isAvailable();
    }

}
